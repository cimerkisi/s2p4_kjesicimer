<?php

	class Menu {
		
		var $html = '
		    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
		    <nav id="sidebar-wrapper">
		        <ul class="sidebar-nav">
		            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
		            <li class="sidebar-brand">
		                <h1>Cimer-ki.si</h1>
		            </li>
		            <li>
		                <a href="<?php echo BASE_URL;?>home/prijava/">Domov</a>
		            </li>
		            <li class="aktiven">
		                <a href="<?php echo BASE_URL;?>iskanje/main_rezultati/">Iskanje</a>
		            </li>
		            <li>
		                <a href="<?php echo BASE_URL;?>home/kdo_smo/">Kdo smo?</a>
		            </li>
		        </ul>
		    </nav>';

		function __construct() {}

		function __toString() {
			return $this->html;
		}
		
	}

?>
