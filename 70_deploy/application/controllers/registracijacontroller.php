<?php

	class RegistracijaController extends Controller {

		function izbira() {
			$this->set("menu", new Menu());
		}

		function osebni_podatki() {
			$reg_data['tip'] = $_POST['tip'];
			$this->set("reg_data", $reg_data);

		}

		function splosni_podatki() {
			$post_data_temp = array(
							"tip",
							"ime",
							"priimek",
							"geslo",
							"mail",
							"spol",
							"datum_rojstva",
							"telefonska_stevilka"
						);
			foreach($post_data_temp as $data) {
				$reg_data[$data] = $_POST[$data];
			}
			$this->set("reg_data", $reg_data);
			$provjera = new Baza();
			$resultTemp = $provjera->query("SELECT * FROM uporabnik WHERE mail LIKE '".mysql_real_escape_string($reg_data['mail'])."'");
			$resultTemp = count($resultTemp);
			if($resultTemp > 0) {
				$this->set("alert", "<script>alert('Vec si registrirani.');window.location.href = ('".BASE_URL."home/prijava/');</script>");
			} else {
				$this->set("alert", "");
			}
		}

		function opis_idealnega_cimra() {
			$post_data_temp = array(
							"tip",
							"ime",
							"priimek",
							"geslo",
							"mail",
							"spol",
							"datum_rojstva",
							"telefonska_stevilka",
							"domaci_kraj",
							"izobrazba",
							"zaposlitev",
							"razmerje",
							"hobiji",
							"glasba",
							"kuham",
							"prehrana",
							"kadilec",
							"ljubljencki",
							"spanje",
							"cistoca",
							"druzabnost"
						);
			if($_POST['tip'] == 2) {
				array_push($post_data_temp, "kraj_nastanitve");
				array_push($post_data_temp, "vselitveno_obdobje_od");
				array_push($post_data_temp, "vselitveno_obdobje_do");
				array_push($post_data_temp, "cas_najema");
				array_push($post_data_temp, "budget");
				array_push($post_data_temp, "opis_jaz_kot_cimer");
			} else if($_POST['tip'] == 3) {
				array_push($post_data_temp, "kraj_nastanitve");
				array_push($post_data_temp, "vselitveno_obdobje_od");
				array_push($post_data_temp, "vselitveno_obdobje_do");
				array_push($post_data_temp, "cas_najema");
				array_push($post_data_temp, "budget");
			}

			foreach($post_data_temp as $data) {
				$reg_data[$data] = $_POST[$data];
			}

			$this->set("reg_data", $reg_data);
		}

		function potrditev() {	
			if($_POST['tip'] == 1) {
			$post_data_temp = array(
							"tip",
							"ime",
							"priimek",
							"geslo",
							"mail",
							"spol",
							"datum_rojstva",
							"telefonska_stevilka"
						);
			} else {
			$post_data_temp = array(
							"tip",
							"ime",
							"priimek",
							"geslo",
							"mail",
							"spol",
							"datum_rojstva",
							"telefonska_stevilka",
							"domaci_kraj",
							"izobrazba",
							"zaposlitev",
							"razmerje",
							"hobiji",
							"glasba",
							"kuham",
							"prehrana",
							"kadilec",
							"ljubljencki",
							"spanje",
							"cistoca",
							"druzabnost"
						);
			}
			if($_POST['tip'] == 2) {
				array_push($post_data_temp, "kraj_nastanitve");
				array_push($post_data_temp, "vselitveno_obdobje_od");
				array_push($post_data_temp, "vselitveno_obdobje_do");
				array_push($post_data_temp, "cas_najema");
				array_push($post_data_temp, "budget");
				array_push($post_data_temp, "opis_jaz_kot_cimer");
			} else if($_POST['tip'] == 3) {
				array_push($post_data_temp, "kraj_nastanitve");
				array_push($post_data_temp, "vselitveno_obdobje_od");
				array_push($post_data_temp, "vselitveno_obdobje_do");
				array_push($post_data_temp, "cas_najema");
				array_push($post_data_temp, "budget");
			}
			
			if(($_POST['tip'] == 2)||($_POST['tip'] == 3)) {
				array_push($post_data_temp, "cimer_s_sobo");
				array_push($post_data_temp, "cimer_spanje");
				array_push($post_data_temp, "cimer_spol");
				array_push($post_data_temp, "cimer_kadilec");
				array_push($post_data_temp, "cimer_cistoca");
				array_push($post_data_temp, "cimer_druzabnost");
			}
/*			$_SESSION['reg_data']['ime'] = $_POST['ime'];
			array_push($_SESSION['reg_data'], )
			$reg_data = new Registracija($_SESSION['reg_data']);*/
			foreach($post_data_temp as $data) {
				$reg_data[$data] = $_POST[$data];
			}
			$this->set("reg_data", $reg_data);
			$upis = new Baza();

			$regMail_str = "Poštovani,\nda bi ste izvršili registraciju potrebno je kliknuti na ";

			$provjera = new Baza();
			$resultTemp = $provjera->query("SELECT * FROM uporabnik WHERE mail LIKE '".mysql_real_escape_string($reg_data['mail'])."'");
			$resultTemp = count($resultTemp);
			if($resultTemp > 0) {
				$this->set("alert", "<script>alert('Vec si registrirani.');window.location.href = ('".BASE_URL."home/prijava/');</script>");
			} else {
				$this->set("alert", "");
	
				switch($_POST['tip']) {
					case 1:
						$registracija = new Najmodavac($reg_data);
						$mail_code = $registracija->returnMailCode();
						$upis->query($registracija);
						$mail = new MailSender($post_data_temp['mail'], "Registracija", $regMail_str . "<a href=\"".BASE_URL."auth/mail_auth/&lbrw=".$mail_code."\">link</a>.", 1);
					break;
					case 2:
						$registracija = new Cimer($reg_data);
						$mail_code = $registracija->returnMailCode();
						$upis->query($registracija);
						$id = $upis->selectMaxID("uporabnik", "id_uporabnik");
						$id = $id['']['MAX(id_uporabnik)'];
						$registracija->lastnostiInsert($id);
						$mail = new MailSender($post_data_temp['mail'], "Registracija", $regMail_str . "<a href=\"".BASE_URL."auth/mail_auth/&lbrw=".$mail_code."\">link</a>.", 1);
					break;
					case 3:
						$registracija = new CimerStanovanje($reg_data);
						$mail_code = $registracija->returnMailCode();
						$upis->query($registracija);
						$id = $upis->selectMaxID("uporabnik", "id_uporabnik");
						$id = $id['']['MAX(id_uporabnik)'];
						$registracija->lastnostiInsert($id);
						$mail = new MailSender($post_data_temp['mail'], "Registracija", $regMail_str . "<a href=\"".BASE_URL."auth/mail_auth/&lbrw=".$mail_code."\">link</a>.", 1);
					break;
				}
			}
		}


	}

?>
