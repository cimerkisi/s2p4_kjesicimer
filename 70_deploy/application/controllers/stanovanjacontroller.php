<?php

	class StanovanjaController extends Controller {
		 
		function dodajanje(){
			session_start();

			$tip_uporabnika=$this->Stanovanja->tip_prijavljen($_SESSION['mail']);
			$tip=$tip_uporabnika['P']['tip_uporabnika'];
			$id_uporabnika=$this->Stanovanja->id_prijavljen($_SESSION['mail']);
			$id=$id_uporabnika['P']['id_uporabnik'];

			if (isset($_POST['dodaj_stanovanje'])){
				
				$this->Stanovanja->sqlInsert($id);
				header('location:'.BASE_URL.'stanovanja/pregled/');

			}

		}
		
		function pregled(){
			session_start();

			if(isset($_POST['submit_upload_image'])){

				$this->Stanovanja->update_image($_POST['stanovanjeid_slika']);
			
				header('location:'.BASE_URL.'stanovanja/pregled/');


			}

			$tip_uporabnika=$this->Stanovanja->tip_prijavljen($_SESSION['mail']);
			$tip=$tip_uporabnika['P']['tip_uporabnika'];
			$this->set('tip_prijavljen',$tip);
			$id_uporabnika=$this->Stanovanja->id_prijavljen($_SESSION['mail']);
			$id=$id_uporabnika['P']['id_uporabnik'];


			$this->set('vsi_prijavljeni', null);
			$this->set('vsi_prijavljeni', $this->Stanovanja->vsi_prijavljeni_stanovanje());
			

			if($tip==3){
				$this->set('stanovanje', $this->Stanovanja->sqlSelect($id));

				$this->set('sobe_izpis',  $this->Stanovanja->sqlSelectSoba());
				//$this->Stanovanja->sqlInsertSoba($_GET['id_stanovanje']);
				//$this->Stanovanja->sqlInsert();
				//$this->Stanovanja->sqlUpdateStanovanje($_GET['id_stanovanje']);
				//$this->Stanovanja->sqlDelete($_GET['id_stanovanje']);

			}

			else{

				header('location:'.BASE_URL.'iskanje/main_rezultati/');
			}
			
			
			
			
		}

		function profil(){
			session_start();

			if(isset($_SESSION['mail'])){
			
			$tip_uporabnika=$this->Stanovanja->tip_prijavljen($_SESSION['mail']);
			$tip=$tip_uporabnika['P']['tip_uporabnika'];
			$this->set('tip_prijavljen',$tip);
			$id_uporabnika=$this->Stanovanja->id_prijavljen($_SESSION['mail']);
			$id=$id_uporabnika['P']['id_uporabnik'];
			$this->set('id_prijavljen',$id);
			$this->set('prijavljen_true',$this->Stanovanja->prijavljen_true($id,$_GET['id_stanovanje'])); //preverim ali je prijavljen, ali če je poslal prosnjo
			
			$this->set('owner_true',$this->Stanovanja->owner_true($id, $_GET['id_stanovanje']));  //preverim ali je lastnik stanovanja

			$this->set('prijavljen', $this->Stanovanja->prijavljen($_SESSION['mail']));
			
			}


			$this->set('vsi_prijavljeni', null);
			$this->set('vsi_prijavljeni', $this->Stanovanja->vsi_prijavljeni_stanovanje());

			$this->set('not_yet_accepted',null);
			$this->set('not_yet_accepted', $this->Stanovanja->not_yet_accepted_stanovanje($_GET['id_stanovanje']));
				


			$this->set('profil_stanovanje', $this->Stanovanja->sqlSelectStanovanje($_GET['id_stanovanje']));
			$this->set('sobe_izpis', $this->Stanovanja->sqlSelectSoba());


			if(isset($_POST['submit_izbrisi_iz_stanovanja'])){ //cimer se izbrise iz stanovanja

				$this->Stanovanja->sql_izbrisi_iz_stanovanja($_POST['id_uporabnik'],$_POST['id_stanovanje']);
				header('location:'.BASE_URL.'stanovanja/profil/&id_stanovanje='.$_GET['id_stanovanje']);

			}

			if(isset($_POST['submit_preklici_prosnjo'])){ //cimer preklice prosnjo
				
				$this->Stanovanja->sql_preklici_prosnjo($_POST['id_uporabnik'],$_POST['id_stanovanje']);
				header('location:'.BASE_URL.'stanovanja/profil/&id_stanovanje='.$_GET['id_stanovanje']);

			}

		

			if(isset($_POST['submit_poslji_prosnjo'])){ //cimer poslje prosnjo
				
				$this->Stanovanja->sql_poslji_prosnjo($_POST['id_uporabnik'],$_POST['id_stanovanje']);
				header('location:'.BASE_URL.'stanovanja/profil/&id_stanovanje='.$_GET['id_stanovanje']);
				

			}

			if(isset($_POST['submit_sprejmi_prosnjo'])){ //najemodajalec sprejme prosnjo --> preveri tip uporabnika!
				
				$this->Stanovanja->sql_sprejmi_prosnjo($_POST['id_uporabnik'],$_POST['id_stanovanje'], $_POST['tip_uporabnika']);
				header('location:'.BASE_URL.'stanovanja/profil/&id_stanovanje='.$_GET['id_stanovanje']);

			}

			if(isset($_POST['submit_zavrni_prosnjo'])){ //najemodajalec zavrne prosnjo
				
				$this->Stanovanja->sql_zavrni_prosnjo($_POST['id_uporabnik'],$_POST['id_stanovanje']);

				header('location:'.BASE_URL.'stanovanja/profil/&id_stanovanje='.$_GET['id_stanovanje']);


			}
			
			
			
		}
		
		function soba(){
		session_start();

			$tip_uporabnika=$this->Stanovanja->tip_prijavljen($_SESSION['mail']);
			$tip=$tip_uporabnika['P']['tip_uporabnika'];
			$id_uporabnika=$this->Stanovanja->id_prijavljen($_SESSION['mail']);
			$id=$id_uporabnika['P']['id_uporabnik'];
			
			
			$this->set('id_stanovanje',$_GET['id_stanovanje']);

			if (isset($_POST['dodaj_sobo'])){
				
				$this->Stanovanja->sqlInsertSoba($_GET['id_stanovanje']);

				header('location:'.BASE_URL.'stanovanja/pregled/');
				
					
			}
			
		}
		
		function urejanje(){
			session_start();

			$tip_uporabnika=$this->Stanovanja->tip_prijavljen($_SESSION['mail']);
			$tip=$tip_uporabnika['P']['tip_uporabnika'];
			$id_uporabnika=$this->Stanovanja->id_prijavljen($_SESSION['mail']);
			$id=$id_uporabnika['P']['id_uporabnik'];




			if($tip==3){

				$this->set('id_stanovanje',$_GET['id_stanovanje']);
			}
			else{

				header('location:'.BASE_URL.'iskanje/main_rezultati/');

			}



			if (isset($_POST['uredi_stanovanje'])){
				
				$this->Stanovanja->sqlUpdateStanovanje($_GET['id_stanovanje']);

				

				header('location:'.BASE_URL.'stanovanja/pregled/');
					
			}
			
			
			
		}
		
		function izbris(){
			session_start();

			$tip_uporabnika=$this->Stanovanja->tip_prijavljen($_SESSION['mail']);
			$tip=$tip_uporabnika['P']['tip_uporabnika'];
			$id_uporabnika=$this->Stanovanja->id_prijavljen($_SESSION['mail']);
			$id=$id_uporabnika['P']['id_uporabnik'];

			if($tip==3){
			
			}
			else{
				header('location:'.BASE_URL.'iskanje/main_rezultati/');
			}


			$this->set('id_stanovanje',$_GET['id_stanovanje']);


			if (isset($_POST['izbrisi'])){
				

				
				$this->Stanovanja->sqlDelete($_GET['id_stanovanje']);
				header('location:'.BASE_URL.'stanovanja/pregled/');
					
			}



			
		}
	}

?>