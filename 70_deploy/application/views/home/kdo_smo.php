<section class="container-fluid naslovna_image col-sm-12 col-xs-12" style="padding:0">
	<div class="container col-md-2 col-sm-3 col-xs-12" style="padding-top:5%;background-color: #333; opacity: 0.8;">
    	<h3 class="text-white text-right">Rabiš cimra? </h3>
    	<h1 style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
    </div>
    
    	<div class="col-md-8 col-sm-12 col-xs-12 col-md-push-1" style="margin-top:10%;background-color:rgba(57,57,57,0.9)">
				<h1 class="text-white text-center" style="background-color:rgba(19,255,0,0.60);padding:5px;">Kdo smo?</h1>
                <div class="container-fluid">
                
                    <div class="col-md-12 text-white">
                    	<h5>    Smo ekipa iz Fakultete za elektrotehniko računalništvo in informatiko.
                            Prav tako smo študenti, ki že nekaj let bivamo v Mariboru. Soočili smo se s sistemom iskanja stanovanj
                            in videli priložnost v tem, da ta sistem izboljšamo. Imeli smo idejo, znanje in željo. Edino kar je še manjkalo je
                            bil začetek dela! Zadeve smo se lotili precej hitro. Bilo je nekaj neuspelih projektov in slabo zastavljenih rešitev.
                            Ampak po dosti vloženega dela in truda smo uspeli izvesti rešitev ki je končna!</h5>
                    
                    </div>
                        <div class="col-md-4 col-sm-4 text-white" style="margin-top:4%;margin-bottom:4%;">
                        <h3><span class="fa fa-group"></span> Skupina ljudi</h3>
                       
                        </div>
                        <div class="col-md-4 col-sm-4 text-white" style="margin-top:4%;margin-bottom:4%;">
                        <h3><span class="fa fa-binoculars"></span> Predvideli problem</h3>
                       
                        </div>
                        <div class="col-md-4 col-sm-4 text-white" style="margin-top:4%;margin-bottom:4%;">
                        <h3><span class="fa fa-check"></span> Izdelala rešitev</h3>
                        
                        </div>
                    
                </div>
                
        </div>
</section>
