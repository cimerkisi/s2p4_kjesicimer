
    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    </script>

<!-- Modal JS -->
<script src="<?php echo BASE_URL; ?>public/js/modal.js"></script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo BASE_URL; ?>public/js/jquery-1.11.3.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo BASE_URL; ?>public/js/bootstrap.js"></script>
</body>
</html>
