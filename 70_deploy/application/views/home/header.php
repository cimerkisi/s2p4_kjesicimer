<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>

<!-- Bootstrap -->
<link href="<?php echo BASE_URL; ?>public/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>public/css/bootstrap-social.css" rel="stylesheet">
<!-- jQuery -->
<script src="<?php echo BASE_URL; ?>public/js/jquery.js"></script>
<!-- Style -->
<link href="<?php echo BASE_URL; ?>public/css/style.css" rel="stylesheet">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,600" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet" type="text/css">

<!-- Font awesome -->
<link rel="stylesheet" href="<?php echo BASE_URL; ?>public/fonts/font-awesome-4.6.3/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
                <h1>Cimer-ki.si</h1>
            </li>
            <li>
                 <a href="<?php echo BASE_URL; ?>iskanje/main_rezultati/">Naslovna</a>
            </li>
            <li>
                 <a href="<?php echo BASE_URL; ?>iskanje/main_rezultati/">Iskanje</a>
            </li>
            <li>
                 <a href="<?php echo BASE_URL; ?>home/kdo_smo/">Kdo smo?</a>
            </li>

            <?php  if(isset($_SESSION['mail'] )) {?>


             <li>
                <a href="<?php echo BASE_URL; ?>cimri/moj_profil/"> <?php echo $_SESSION['mail']?> </a>
            </li>

            <li>
                <a href="<?php echo BASE_URL; ?>home/odjava/">Odjava</a>
            </li>

          

            <?php } else { ?>

             <li>
                <a href="<?php echo BASE_URL; ?>home/prijava/">Prijava</a>
            </li>


           <?php } ?>



        </ul>
    </nav>

