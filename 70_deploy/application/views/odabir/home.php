<section class="container-fluid naslovna_image col-sm-6 col-xs-12">
	<div class="container col-md-6 col-sm-10 col-xs-12" style="float:right;padding-top:20%;background-color: #333; opacity: 0.8;">
    	<h3 class="text-white text-right"> Rabiš cimra? </h3>
    	<h1 style="float:right"> <img src="image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
    </div>
    
    
    <div class="container col-md-6 col-sm-10 col-xs-12 hidden-lg hidden-md hidden-sm" style="height:20px;"></div>
    <div class="container col-md-6 col-sm-10 col-xs-12 hidden-lg hidden-md hidden-sm" style="float:left;padding-top:20%;padding: 3%; opacity: 0.8;background-color:#333">
    	<h3  class="text-white"> Ni problema, <small> prijavi se in najdi svojega idealnega <strong>cimra</strong>! Če pa nimaš korisničkega računa se lahko <strong>registriraš <a href="registracija.html" class="alert-warning">tukaj</a></strong>. </small> </h3>
        
        						<form action="" method="">
    	 								<div class="form-group">
                                            <label class="text-white">Elektronski naslov</label>
                                            <input type="email" class="form-control" placeholder="E-naslov">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="text-white">Geslo</label>
                                            <input type="password" class="form-control" placeholder="geslo">
                                        </div>
                                        
                                        <div class="form-group" style="float:right;">
                                            <button type="submit" class="btn btn-outline btn-primary"> Prijavi se</button>
                                        </div>
                                 </form>
                                 
                                 
    <div style="padding-top:3%;float:right">
    	<h3 class="text-white"> Ali, <small> lahko se tudi prijaviš z tvojim facebook računom</small></h3>
                            <button class="btn btn-social btn-facebook">
                                <i class="fa fa-facebook"></i> Prijavi se
                            </button>
    </div>
    </div>
</section>
  
<section class="container-fluid col-sm-6 col-xs-12 hidden-xs">
	<div class="container col-md-6 col-sm-10 col-xs-12" style="float:left;padding-top:20%;background-color: #fff; opacity: 0.8;">
    	<h3> Ni problema, <small> prijavi se in najdi svojega idealnega <strong>cimra</strong>! Če pa nimaš korisničkega računa se lahko <strong>registriraš <a href="registracija.html" class="alert-success">tukaj</a></strong>. </small> </h3>
        
        						<form action="" method="">
    	 								<div class="form-group">
                                            <label>Elektronski naslov</label>
                                            <input type="email" class="form-control" placeholder="E-naslov">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Geslo</label>
                                            <input type="password" class="form-control" placeholder="geslo">
                                        </div>
                                        
                                        <div class="form-group" style="float:right;">
                                            <button type="submit" class="btn btn-outline btn-default"> Prijavi se</button>
                                        </div>
                                 </form>
                                 
                                 
    <div style="padding-top:3%;float:right">
    	<h3> Ali, <small> lahko se tudi prijaviš z tvojim facebook računom</small></h3>
                            <button class="btn btn-social btn-facebook">
                                <i class="fa fa-facebook"></i> Prijavi se
                            </button>
    </div>
    </div>
    
</section> 
