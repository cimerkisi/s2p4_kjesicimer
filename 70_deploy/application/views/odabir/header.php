<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>

<!-- Bootstrap -->
<link href="../../public/css/bootstrap.css" rel="stylesheet">
<link href="../../public/css/bootstrap-social.css" rel="stylesheet">

<!-- Style -->
<link href="../../public/css/style.css" rel="stylesheet">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,600" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet" type="text/css">

<!-- Font awesome -->
<link rel="stylesheet" href="../../public/fonts/font-awesome-4.6.3/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <!-- Navigation -->
    <a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
                <h1>Cimer-ki.si</h1>
            </li>
            <li>
                <a href="home.html">Naslovna</a>
            </li>
            <li class="aktiven">
                <a href="iskanje.html">Iskanje</a>
            </li>
            <li>
                <a href="#kdoSmo">Kdo smo mi?</a>
            </li>
        </ul>
    </nav>
