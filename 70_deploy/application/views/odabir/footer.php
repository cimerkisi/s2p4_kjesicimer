    <!-- jQuery -->
    <script src="../../public/js/jquery.js"></script>

    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    </script>

<!-- Modal JS -->
<script src="../../public/js/modal.js"></script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="../../public/js/jquery-1.11.3.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="../../public/js/bootstrap.js"></script>
</body>
</html>
