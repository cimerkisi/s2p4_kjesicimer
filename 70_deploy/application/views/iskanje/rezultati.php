<section class="container-fluid naslovna_image_autoH col-sm-12 col-xs-12" style="padding:0;">

	<div class="container-fluid col-sm-12 col-xs-12" style="background-color: #333;">
    
        <div class="row">
        
                <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="col-sm-2" alt="Cimer-ki.si" style="padding-top:5px;"/>
            
                <div class="col-sm-5">
                      <form role="search" style="padding-top:10px;">
                             <div class="input-group col-sm-12" style="padding-bottom:10px;">
                                <input type="text" class="form-control search-input" placeholder="Vpisite ime mesta kje iscete">
                                 <span class="input-group-btn">
                                    <button class="btn btn-default search-icon" type="button"><span class="fa fa-search"></span></button>
                                  </span>
                             </div>
                        </form>
                </div>
                
                 <div class="input-group col-sm-1 col-xs-12" role="tab" id="headingOne" style="padding-top: 10px;">
                        <button class="btn btn-outline btn-success col-xs-12" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                          Napredno iskanje <i class="fa fa-angle-double-down"></i>
                        </button>
                </div>
            
       </div>
       
    <!-- FILTER -->
    <div id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne"  style="background-color: #333;">
          <div class="panel-body text-white">
           <h4> FILTER:</h4>
           
                       <form method="" action="">
                       
                           <fieldset class="col-sm-6">
                       		<h3> Kriteriji sobe</h3>
                            <hr class="hr-dark">
                            
                            	<form>
                                    <label>Iskanje Soba:</label>
                                    
                                    <div class="">
                                        <label>Stevilo postelj: </label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_postelj" value="1">1
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_postelj" value="2">2
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_postelj" value="3">3
                                		</label>
                                        <!-- �E JE VE� KOT 3, POTEM SE ODPRE TEXT ZA VPIS �TEVILA POSTELJ-->
                                        <input type="text" class="form-control" name="st_postelj" placeholder="Ni navedeno.."/>
                                    </div>
                                
                                    <div class="">
                                        <label>Stevilo cimrov:</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_cimrov" value="1">1
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_cimrov" value="2">2
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_cimrov" value="3">3
                                		</label>
                                        <!-- �E JE VE� KOT 3, POTEM SE ODPRE TEXT ZA VPIS �TEVILA CIMROV-->
                                        <input type="text" class="form-control" name="st_cimrov" placeholder="Ni navedeno.."/>
                                    </div>
                                
                                    <div class="">
                                        <label>Stevilo praznih mest:</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_praznih_mest" value="1">1
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_cimrov" value="2">2
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="st_cimrov" value="3">3
                                		</label>
                                        <!-- �E JE VE� KOT 3, POTEM SE ODPRE TEXT ZA VPIS �TEVILA POSTELJ-->
                                        <input type="text" class="form-control" name="st_praznih_mest" placeholder="Ni navedeno.."/>
                                    </div>
                                
                                    <div class="form-group">
                                        <label>Najemnina sobe:</label>
                                        <input type="range" class="form-control" name="najemnina_soba" min="0" value="10" max="600" step="10"> EUR
                                    </div>
                                
                                    <div class="form-group">
                                        <label>Velikost sobe:</label>
                                        <input type="range" class="form-control" name="velikost_soba" min="0" value="10" max="600" step="10"> m^2
                                        
                                    </div>
                                
                                    <div class="">
                                        <label>Zeljeni spol:</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="spol" value="M">Moski
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="spol" value="Z">Zenski
                                		</label>
                                        <label class="radio-inline">
                                    		<input type="radio" name="spol" value="V">Vseeno
                                		</label>
                                    </div>
                                    
                                    
                                
                                    <!-- najemnina stanovanja se ra�una iz najemnin posameznih sob(�e ni sob, potem se napi�e direktno v stanovanje) -->
                                
                                    </form>
                            
                           
                           </fieldset>
                           
                           <fieldset class="col-sm-6">
                           <h3> Kriteriji stanovanja</h3>
                           <hr class="hr-dark">
                           
                           <form class="form-control-static">
                                
                                <div class="form-group">
                                <label>Mesto: </label>
                                <input class="form-control" type="text" name="naziv_mesta"/>
                                </div>
                            
                                
                                <div class="form-group">
                                <label>Velikost stanovanja:</label>
                                <input type="text" class="form-control" name="velikost_stanovanja">
                                </div>
                            
                                <div class="">
                                    <label>Stevilo sob: </label>
                                    <label class="radio-inline">
                                    	<input type="radio" name="st_sob" value="1">1
                                    </label>
                                    <label class="radio-inline">
                                    	<input type="radio" name="st_sob" value="2">2
                                    </label>
                                    <label class="radio-inline">
                                    	<input type="radio" name="st_sob" value="3">3
                                    </label>
                                    <!-- �E JE VE� KOT 3, POTEM SE ODPRE TEXT ZA VPIS �TEVILA POSTELJ-->
                                    <input type="text" class="form-control" name="st_sob" placeholder="Nic od navedenega.."/>
                                </div>
                            
                                
                            
                                <!-- najemnina stanovanja se racuna iz najemnin posameznih sob(�e ni sob, potem se napi�e direktno v stanovanje) -->
                                <div class="form-group">
                                    <label>Najemnina stanovanje:</label>
                                    <input type="range" class="form-control" name="najemnina_soba" min="0" value="10" max="600" step="10"> EUR
                                </div>
                            
                                <div class="">
                                <label>Zeljeni spol:</label>
                                <label class="radio-inline">
                                    	<input type="radio" name="spol" value="M">Moski
                                </label>
                                <label class="radio-inline">
                                    	<input type="radio" name="spol" value="Z">Zenski
                                </label>
                                <label class="radio-inline">
                                    	<input type="radio" name="spol" value="V">Vseeno
                                </label>
                                </div>
                            
                                <div class="">
                                <label>Oprema stanovanja:</label>
                                <label class="radio-inline">
                                    	<input type="radio" name="oprema" value="internet">Internet
                                </label>
                                <label class="radio-inline">
                                    	<input type="radio" name="oprema" value="televizija">Televizija
                                </label>
                                <label class="radio-inline">
                                    	<input type="radio" name="oprema" value="klima">Klima
                                </label>
                                <label class="radio-inline">
                                    	<input type="radio" name="oprema" value="hladilnik">Hladilnik
                                </label>
                                <label class="radio-inline">
                                    	<input type="radio" name="oprema" value="omara">Omara
                                </label>
                                </div>
                            
                                                        
                                </form>
                           
                           </fieldset>
                          
                          <div class="form-group">
                               <button class="btn btn-outline btn-group-justified btn-lg btn-default"><span class="fa fa-search"></span> Najdi</button>
                          </div>
                      </form>
            </div>
    	</div>
    </div>


<div class="container-fluid" style="background-color: #f4f4f4; margin-top:4%; opacity: 1">

    <hgroup class="mb20">
		<h3>Rezultati iskanja</h3>
		<h2 class="lead"><strong class="text-danger">3</strong> rezultati je najdeno za iskanje <strong class="text-danger">Lorem</strong></h2>								
	</hgroup>

    <section class="col-xs-12 col-sm-12 col-md-12">
    
    <!-- rezultat iskanja -->
		<article class="search-result row">
        
			<div class="col-xs-12 col-sm-12 col-md-3">
				<a href="stanovanja_profil.html" title="Lorem ipsum" class="thumbnail"><img src="image/ap_1.jpg" alt="Lorem ipsum" /></a>
			</div>
            
			<div class="col-xs-12 col-sm-12 col-md-2">
				<ul class="meta-search">
					<li><i class="fa fa-wifi"></i> <span> Wi-fi</span></li>
					<li><i class="fa fa-institution"></i> <span> V blizini fakultet</span></li>
					<li><i class="fa fa-car"></i> <span> Parkirni prostor</span></li>
					<li><i class="fa fa-map-marker fa-2x"></i> <span> Dobra lokacija</span></li>
				</ul>
			</div>
            
			<div class="col-xs-12 col-sm-12 col-md-6 excerpet">
				               
                 <!-- CIMER -->
                            <div onClick="location.href='uporabniski_profil.html'" class="profile-header-container col-lg-2 col-md-4 col-sm-3 col-xs-6">   
                                <div class="profile-header-img">
                                    <img class="img-circle" src="image/avatar-1299805_640.png" alt="" />
                                    <!-- badge -->
                                    <div class="rank-label-container">
                                        <span class="label label-default rank-label">89%</span>
                                    </div>
                                </div>
                            </div>
                            
                   <!-- CIMER -->
                            <div onClick="location.href='#'" class="profile-header-container col-lg-2 col-md-4 col-sm-3 col-xs-6">   
                                <div class="profile-header-img">
                                    <img class="img-circle" src="image/q.png" alt="" />
                                    <!-- badge -->
                                    <div class="rank-label-container">
                                        <span class="label label-default rank-label">89%</span>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                
			</div>
            <button id="myBtn" class="btn btn-outline plus"><i class="fa fa-envelope fa-4x"></i></button>
            
            <!-- MODAL SPORO�ANJE -->
            
            
            <!-- The Modal -->
            <div id="myModal" class="modal">
            
              <!-- Modal content -->
            <div class="modal-content">
              <div class="modal-header">
                <span class="close">�</span>
                <h2>Modal Header</h2>
              </div>
              <div class="modal-body">
                <p>Some text in the Modal Body</p>
                <p>Some other text...</p>
              </div>
              <div class="modal-footer">
                <h3>Modal Footer</h3>
              </div>
            </div>
            
            </div>
            
			<span class="clearfix borda"></span>
            
            <hr class="hr-short">
		</article>
<!-- konec rezultata iskanja -->

       
	</section>
	
</div>

</section>