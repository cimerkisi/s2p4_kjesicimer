<section class="container-fluid naslovna_image_autoH col-xs-12" style="padding:0">

	<div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">UREJANJE STANOVANJA</h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> UREJANJE STANOVANJA </h3>
    </div>
   
  
	<div class="hidden-sm hidden-md hidden-lg" style="margin-top:30%;"></div>

    <!-- BOX -->
   <div class="container" style="margin-top:2%;margin-bottom:5%;">
                     
        <div class="col-sm-10 text-center col-sm-push-1" style="background-color: #333; opacity: 0.9;padding:1%;margin-top: 5%">
        
            <div class="col-sm-12 text-white">
           			
                    <h2> Spreminjanje stanovanja</h2>
                    <hr class="hr-dark">
                    <!-- action="<?php //echo BASE_URL."stanovanja/pregled/"; ?>" -->
					<form method="POST" class="form-horizontal">
                    <!--
                        <div class="form-group">
                            <label class="control-label col-sm-4">Izberite sliko:</label>
                            <img id="slika" src="#" class="img-responsive img-thumbnail" alt="Prikaz slike"/>
                                    <label id="galerija_slik" hidden="hidden"> Izberite sliko:</label>
                                    <div class="col-sm-4">
                                        <input onchange="readURL(this);" type="file" name="img_source" accept="image/*" class="file btn btn-outline btn-violcno" oninvalid="this.setCustomValidity('Potrebno je dodati vsaj sliko!')" oninput="setCustomValidity('')" required>
                                    </div>
                        </div>
                         -->      
                         
                         
                  <div class="form-group">
                        <label class="control-label col-sm-3">Mesto / Poštna številka</label>
                        <div class="col-sm-3">
                        
                        	<input class="form-control" type="text" name="naziv" value="Maribor" placeholder="Mesto">
                        </div>
                         <label class="control-label col-sm-1" style="text-align: center">  </label>
                        <div class="col-sm-3">
                            <input class="form-control" type="number" value="0" name="postna_stevilka" placeholder="Poštna številka">
                        </div>
                        
                    </div>      
                    <div class="form-group">
                        <label class="control-label col-sm-3">Naslov / Hišna številka</label>
                        <div class="col-sm-3">
                        
                        	<input class="form-control" type="text" value="Naslov" name="naslov" placeholder="Naslov">
                        </div>
                         <label class="control-label col-sm-1" style="text-align: center">  </label>
                        <div class="col-sm-3">
                            <input class="form-control" type="text" value="0" name="hisna_stevilka" placeholder="Hišna številka">
                        </div>
                        
                    </div> 
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Predel:</label>
                        <div class="col-sm-7">
                        
                        	<input class="form-control" type="text" name="predel">
                        </div>
                                                
                    </div>
                    
                    <hr class="hr-dark"> 
                    <div class="form-group">
                        <label class="control-label col-sm-3">Najemnina stanovanja (na mesec):</label>
                        <div class="col-sm-7">
                         <div class="input-group">
                        	  
                                    <input type="number" value="0" name="najemnina" class="form-control input-mini" min="0" style="-moz-appearance:textfield;">
                                    <span class="input-group-addon"><i class="fa fa-eur"></i></span>
                         </div>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Stroški stanovanja (na mesec):</label>
                        <div class="col-sm-7">
                        	 <div class="input-group">
                        	  
                                    <input type="number" value="0" name="stroski" class="form-control input-mini" min="0" style="-moz-appearance:textfield;">
                                    <span class="input-group-addon"><i class="fa fa-eur"></i></span>
                         
                            </div>
                        </div>
                    </div>
                    <div class="control-label col-sm-12">
                    	
                                <label class="control-label col-sm-3">Ali so stroški fiksni:</label>
                                <div class="col-sm-7 col-xs-12 input-group">
                                <select class="form-control" name="stroski_fiksni" required>
                                    <option value="1" selected>Fiksni</option>
                                    <option value="0">Spremenljivi</option>
                                </select>
                                </div>
                                
                            </div>
                    
                    <hr class="hr-dark"> 
                    	
				<div class="form-group">
                        <label class="control-label col-sm-3">Vselitveno obdobje:</label>
                        <div class="col-sm-3">
                        
                        	<input class="form-control" value="2016-05-31" type="date" name="vselitveno_obdobje_od" placeholder="mm/dd/yyyy">
                        </div>
                         <label class="control-label col-sm-1" style="text-align: center"> - </label>
                        <div class="col-sm-3">
                            <input class="form-control"  value="2016-05-31" type="date" name="vselitveno_obdobje_do" placeholder="mm/dd/yyyy">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Čas najema v mesecih</label>
                        <div class="col-sm-7">
                        
                        	<input class="form-control"  value="0" type="number" name="cas_najema">
                        </div>
                                                
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Velikost stanovanja:</label>
                        <div class="col-sm-7">
                         <div class="input-group">
                        	  
                                    <input type="number" value="0"  name="velikost_stanovanja" class="form-control input-mini"
                                            min="0" style="-moz-appearance:textfield;">
                                    <span class="input-group-addon">m2</span>
                         
                            </div>
                        </div>
                                                
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Letnica izgradnje</label>
                        <div class="col-sm-7">
                        
                        	<input class="form-control" type="month" name="letnica_izgradnje" placeholder="yyyy">
                        </div>
                                                
                    </div>
                    <hr class="hr-dark">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Število sob</label>
                        <div class="col-sm-7">
                        
                        	<input class="form-control" value="0"  type="number" name="stevilo_sob" placeholder="3,4..">
                        </div>
                    </div>
                        
                    <div class="form-group">
                        <label class="control-label col-sm-3">Oprema stanovanja</label>
                        <div class="col-sm-7">
                        
                        	<input class="form-control" type="text" name="oprema_stanovanja" placeholder="Internet, parkirni prostor....">
                        </div>
                                                
                    </div>
                    <div class="form-group">
                     <label class="control-label col-sm-3">Ustrezno označite</label>
                        <div class="col-sm-3 text-left">
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="internet">Internet</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="parkirni_prostor">Parkirni prostor</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="center_mesta">Center mesta</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="blizina_fakultet">Bližina fakultet</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="blizina_avtobusne_postaje">Bližina avtobusne postaje</label>
                            </div>
                       </div>
                       <div class="col-sm-3 text-left">
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="blizina_trgovin">Bližina trgovin</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="prijazno_invalidom">Prilagojen prostor</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="prijazno_invalidom">Prijazno invalidom</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="pralni_stroj">Pralni stroj</label>
                            </div>
                            <div class="checkbox">
                              <label><input type="checkbox" name="checkbox[]" value="blizina_knjiznic">Bližina knjiznic</label>
                            </div>
                       </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Opis</label>
                        <div class="col-sm-7">
                        
                        	<textarea class="form-control" type="month" name="oprema"></textarea>
                        </div>
                                                
                    </div>
                    
                    <div class="form-group col-sm-12" style="margin-top:2%">
                    <button type="submit" name="uredi_stanovanje" class="btn btn-outline btn-violcno" style="float:right;"><span class="fa fa-check"></span> Potrdi</button>
                    </div>
                    
                        </form>


            
        	</div>
        
        </div>
               
 	</div>

   <div class="col-xs-12" style="height:120px;"></div>
</section>