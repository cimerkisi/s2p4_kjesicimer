<section class="container-fluid naslovna_image_autoH col-xs-12" style="padding:0">

   <div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">DODAJANJE SOBE</h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> DODAJANJE SOBE </h3>
    </div>
   
  
  
	<div class="hidden-sm hidden-md hidden-lg" style="margin-top:30%;"></div>

    <!-- BOX -->
      <div class="container" style="margin-top:2%;margin-bottom:5%;">
                     
        <div class="col-sm-10 text-center col-sm-push-1" style="background-color: #333; opacity: 0.9;padding:1%;margin-top: 5%">
        
            <div class="col-sm-12 text-white">
           			
                    <h2> Dodajte sobo</h2>
                    <hr class="hr-dark">

<!-- action="<?php echo BASE_URL."stanovanja/pregled/&id_stanovanje=".$id_stanovanje; ?>"-->
    <form method="POST"  action="" class="form-horizontal">
                    <!-- 
                    	<div class="form-group">
                            <label class="control-label col-sm-4">Izberite sliko:</label>
                            <img id="slika" src="#" class="img-responsive img-thumbnail" alt="Prikaz slike"/>
                                    <label id="galerija_slik" hidden="hidden"> Izberite sliko:</label>
                                    <div class="col-sm-4">
                                        <input onchange="readURL(this);" type="file" name="img_source" accept="image/*" class="file btn btn-outline btn-violcno" oninvalid="this.setCustomValidity('Potrebno je dodati vsaj sliko!')" oninput="setCustomValidity('')" required>
                                    </div>
                        </div>
                               -->
                            
                             <div class="control-label col-sm-12">
                                <label class="col-sm-4">Število postelj: </label>
                               <div class="col-sm-8 col-xs-12 input-group">
                                <select class="form-control" name="stevilo_postelj" required>
                                    <option value="1" selected>1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                </div>
                                
                            </div>
                        
                            <div class="control-label col-sm-12">
                                <label class="col-sm-4">Število praznih mest: </label>
                               <div class="col-sm-8 col-xs-12 input-group">
                                <select class="form-control" name="stevilo_praznih_mest" required>
                                    <option value="1" selected>1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                </div>
                                
                            </div>
                            
                           
                           
                            <div class="control-label col-sm-12">
                    	
                                <label class="col-sm-4">Spol:</label>
                                <div class="col-sm-8 col-xs-12 input-group">
                                <select class="form-control" name="zeljeni_spol" required>
                                    <option value="z" selected>Ženski</option>
                                    <option value="m">Moški</option>
                                </select>
                                </div>
                                
                            </div>
                            <div class="control-label col-sm-12">
                                        <label class="col-sm-4">Oprema sobe:</label>
                                        <div class="col-sm-8 col-xs-12 input-group">
                                        <textarea type="text" class="form-control" name="oprema"> </textarea>
                                        </div>
                            </div>
                            
                            <div class="control-label col-sm-12">
                                        <label class="col-sm-4">Opis sobe:</label>
                                        <div class="col-sm-8 col-xs-12 input-group">
                                        <textarea type="text" class="form-control" name="opis_sobe"> </textarea>
                                        </div>
                            </div>
                        
                            <!-- najemnina stanovanja se računa iz najemnin posameznih sob(če ni sob, potem se napiše direktno v stanovanje) -->
                                <div class="control-label col-sm-12" style="margin-top:5%;">
                                    <button type="submit" name="dodaj_sobo" class="btn btn-primary"><i class="fa fa-check"></i> dodaj sobo</button>
                                </div>
                            </form>
            
        	</div>
        
        </div>
               
 	</div>

   <div class="col-xs-12" style="height:120px;"></div>
</section>