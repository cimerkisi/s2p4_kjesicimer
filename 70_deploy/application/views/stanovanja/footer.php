<div class="container-fluid" style="padding:0;background-color:rgba(58,58,58,0.95);color:#eee;">
	<div class="container-fluid" style="background-color:rgba(58,58,58,1.00);height:10px;"></div>
    
    <div class="col-md-4 col-sm-12 text-center" style="padding:50px 20px 10px 20px;margin-bottom:5%">
      <h1><img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
      
      <hr class="hr-dark">
         <h4><span class="fa fa-envelope"> </span> testni@email-naslov.com </h4> 
         <h4><span class="fa fa-phone"> </span> +3902 1279 12</h4>
      <button type="submit" class="btn btn-outline btn-violcno">O projektu</button>
      <button type="submit" class="btn btn-success">O nas</button>
      
    </div>
    <div class="col-md-8 col-sm-12 text-center" style="padding:70px 10px 10px 10px;margin-bottom:5%">
    	 <h4 class="text-left">Zakaj Cimer ki si?</h4>
         <hr class="hr-dark">
         	<p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam venenatis luctus purus vitae elementum. Morbi lacus risus, volutpat ut purus eget, tempor laoreet ex. In nec neque sit amet nisi consequat lobortis. Duis nunc odio, luctus sed metus rutrum, pulvinar</p><p> ullamcorper urna. Suspendisse id blandit tellus. Suspendisse facilisis, ligula et posuere facilisis, risus lectus molestie libero, ut eleifend metus sapien et enim. Proin nisi tortor, facilisis in sapien sed, consequat vehicula nulla. Quisque rutrum ex lorem. Pellentesque sed odio rhoncus, consectetur lectus at, accumsan erat. Proin eget sollicitudin mauris, tempor molestie lectus.</p>
            
    </div>
<div class="container-fluid col-sm-12 text-center" style="background-color:rgba(58,58,58,1.00);">
	Copyright 2016 c Cimer-ki.si
</div>
</div>
    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    </script>

<!-- Modal JS -->
<script src="<?php echo BASE_URL; ?>public/js/modal.js"></script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo BASE_URL; ?>public/js/jquery-1.11.3.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo BASE_URL; ?>public/js/bootstrap.js"></script>
</body>
</html>
