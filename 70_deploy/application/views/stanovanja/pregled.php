<section class="container-fluid naslovna_image_autoH col-xs-12" style="padding:0">

	<div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">VAŠA STANOVANJA</h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> VAŠA STANOVANJA </h3>
    </div>
   
	<div class="hidden-sm hidden-md hidden-lg" style="margin-top:30%;"></div>
  
    <!-- BOX -->
   <div class="container" style="margin-top:2%;margin-bottom:5%">
                     
        <div class="col-sm-10 text-center col-sm-push-1" style="background-color: #333; opacity: 0.9;padding:1%;margin-top: 5%">
        
            <div class="col-sm-12 text-white">
            <!-- BUTTON ZA DODAJANJE STANOVANJ -->
            <h2 class="text-right">  
            
            	<a href="<?php echo BASE_URL . "stanovanja/dodajanje/"; ?>" id="registracija_2" class="btn btn-success"><span class="fa fa-plus"></span> DODAJ STANOVANJE</a>
            
            </h2>
            <hr class="hr-dark">
        
        <?php if (is_null($stanovanje)){ ?>
        <div class="col-sm-12" style="margin-bottom: 6%;margin-top:5%;">
			<h1> NIMATE DODANIH STANOVANJ</h1>
            <h4>- če želite dodati stanovanje, lahko ga dodate pritiskom na gumb dodaj stanovanje.</h4>
            
        </div>
            <hr class="hr-dark" style="margin-bottom:10%">
		<?php }else{ 
		
		foreach ($stanovanje as $value) {  ?>
        
        
        <!-- stanovanje začetek -->
        <article class="search-result row" style="background-color: #747474; padding: 5px;">
        
        <!-- naziv stanovanja -->
        
        <h2 class="text-left"><?php echo $value['K']['naziv'];?> <?php echo $value['K']['postna_stevilka'];?>, <?php echo $value['N']['ulica'];?> <?php echo $value['N']['hisna_stevilka'];?>,<br /><small class="text-white"> Vselitveno obdobje: <?php echo $value['U']['vselitveno_obdobje_od'];?> - <?php echo $value['U']['vselitveno_obdobje_do'];?></small></h2>
        
			<div class="col-xs-12 col-sm-12 col-md-3">
            <form action="<?php echo BASE_URL.'stanovanja/profil/&id_stanovanje='.$value['U']['id_stanovanje']; ?>" method="GET" id="forma">
				<button type="submit" class="btn btn-default btn-outline"><img src="<?php echo $value['U']['slika'];?>" alt="stanovanje_slika" class="img-responsive"/></button>
                </form>
                
                <!-- slika upload -->
                
                   <form method="POST" action="" enctype="multipart/form-data">
                       <input type="hidden" value='<?php echo $value['U']['id_stanovanje'];?>' name="stanovanjeid_slika">                        
                         
                          <div class="form-group" style="margin-top:2%">
                              <input  placeholder="Vnesite url slike" class="form-control" value='<?php echo $value['P']['slika']?>' type="text" name="slika_link">
                        
                          </div> 
                         
                        
                        <div class="form-group" style="margin-top:2%">

                            <button id="btn" name="submit_upload_image" type="submit" class="btn btn-outline btn-success" style="float:right"><i class="fa fa-check" aria-hidden="true"></i>Shrani</button>

                                                
                        </div>  
                      </form> 
                
			</div>
            
            <!-- MODALS -->
			<div class="col-xs-12 col-sm-12 col-md-1">
				<ul class="meta-search text-left">
					<li><form action="<?php echo BASE_URL.'stanovanja/soba/&id_stanovanje='.$value['U']['id_stanovanje']; ?>" method="GET" id="forma"><button type="submit" class="btn btn-warning btn-xs"><span class="fa fa-building-o"></span></button></form></li>
                        
				
                    <li><form action="<?php echo BASE_URL.'stanovanja/profil/&id_stanovanje='.$value['U']['id_stanovanje']; ?>" method="GET" id="forma"><button type="submit" class="btn btn-primary btn-xs"><span class="fa fa-group"></span></button></form></li>
                    
					<li><form method="GET" action="<?php echo BASE_URL.'stanovanja/urejanje/&id_stanovanje='.$value['U']['id_stanovanje'].''; ?>"><button class="btn btn-success btn-xs" type="submit"><span class="fa fa-edit"></span>
                    
                    </button></form></li>
                    <li><form id="form" method="GET" action="<?php echo BASE_URL."stanovanja/izbris/&id_stanovanje=".$value['U']['id_stanovanje'];?>"><button class="btn btn-danger btn-xs" type="submit"><span class="fa fa-remove"></span></button></form></li>
                    
                   </ul>
			</div>
            <div class="col-xs-12 col-sm-12 col-md-3">
             <!-- OPIS STANOVANJA -->
                       <small class="text-left">
                       <h4>Splošne informacije</h4>
                       		<h5>Predel: <?php echo $value['U']['predel'];?></h5>
                       		<h5>Stroški:  <?php echo $value['U']['stroski'];?> <span class="fa fa-euro"></span></h5>
                       		<h5>Najemnina:  <?php echo $value['U']['najemnina'];?>  <span class="fa fa-euro"></span></h5>
                       		<h5>Čas najema: <?php echo $value['U']['cas_najema'];?> mesec/i</h5>
                       		<h5>Velikost: <?php echo $value['U']['velikost_stanovanja'];?></h5>
                       		<h5>Izgradnja: <?php echo $value['U']['letnica_izgradnje'];?></h5>
                       		<h5><strong>Število sob:  <?php echo $value['U']['stevilo_sob'];?></strong></h5>
                       		<h5>Oprema: <?php echo $value['U']['oprema'];?></h5>
                       
                       </small>
              
			</div>
            <div class="col-xs-12 col-sm-12 col-md-4 excerpet">
            <small>
            	<h4>Oprema</h4>
                
                <?php if($value['U']['internet']==1){;?>
                
                <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-wifi"></span></h2>
                        <h4>Internet</h4>
                </div>
                <?php } else { ?>
                
					 <div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-wifi"></span></h2>
                        <h4>Internet</h4>
               		 </div>
					
					
					<?php }if($value['U']['center_mesta']==1){;?>
                <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-map-marker"></span></h2>
                        <h4>Center</h4>
                </div>
                
               <?php } else { ?>
                
					 <div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-map-marker"></span></h2>
                        <h4>Center</h4>
                </div>
					
					
					<?php } if($value['U']['blizina_fakultet']==1){;?>
                <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-graduation-cap"></span></h2>
                        <h4>Bližina fakultet</h4>
                </div>
                
                <?php }else { ?>
                
					 <div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-graduation-cap"></span></h2>
                        <h4>Bližina fakultet</h4>
                </div>
					
					
					<?php }if($value['U']['parkirni_prostor']==1){ ;?>
                 <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-car"></span></h2>
                        <h4>Parkirni prostor</h4>
                </div>
                
                <?php }else { ?>
                
					<div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-car"></span></h2>
                        <h4>Parkirni prostor</h4>
                </div>
					
					
					<?php } if($value['U']['blizina_trgovin']==1){;?>
                 <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-shopping-basket"></span></h2>
                        <h4>Bližina trgovin</h4>
                </div>
                
                <?php }else { ?>
                
					<div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-shopping-basket"></span></h2>
                        <h4>Bližina trgovin</h4>
                </div>
					
					
					<?php  } if($value['U']['prijazno_invalidom']==1){;?>
                 <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-wheelchair"></span></h2>
                        <h4>Prijazno invalidom</h4>
                </div>
                
                <?php }else { ?>
                
					<div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-wheelchair"></span></h2>
                        <h4>Prijazno invalidom</h4>
                </div>
					
					
					<?php }if($value['U']['pralni_stroj']==1){;?>
                 <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-check-circle"></span></h2>
                        <h4>Pralni stroj</h4>
                </div>
                
                <?php }else { ?>
                
					<div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-check-circle"></span></h2>
                        <h4>Pralni stroj</h4>
                </div>
					
					
					<?php }if($value['U']['blizina_knjiznic']==1){;?>
                 <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-university"></span></h2>
                        <h4>Bližina knjižnic</h4>
                </div>
                
                <?php }else { ?>
                
					<div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-university"></span></h2>
                        <h4>Bližina knjižnic</h4>
                </div>
					
					
					<?php }if($value['U']['blizina_avtobusne_postaje']==1){;?>
                 <div class="col-md-4">
                        <h2 style="padding:15px;"><span class="fa fa-bus"></span></h2>
                        <h4>Blizina avtobusne</h4>
                </div>
                
                <?php }else { ?>
                
					<div class="col-md-4" style="color:#333;">
                        <h2 style="padding:15px;"><span class="fa fa-bus"></span></h2>
                        <h4>Blizina avtobusne</h4>
                </div>
					
					
					<?php }?>
                
            </small>
            </div>
            
			<div class="col-xs-12 col-sm-12 col-md-12 excerpet" style="padding-top:3%;padding-bottom:3%">
				               
            	<h4 class="text-left">Stanovalci:</h4>


                        <?php  if($vsi_prijavljeni!=null){

                          foreach ($vsi_prijavljeni as $stanovalec) {

                                  if($stanovalec['Prijave']['id_stanovanje']==$value['U']['stanovanje_id_stanovanje']) {?>


                                    <div class="profile-header-container col-lg-4 col-md-5 col-sm-3 col-xs-6">   
                                        <div class="profile-header-img">
                                              <?php  echo ' <img class="img-circle" src="'.$stanovalec['Upo']['slika'].'" alt="" style=" border: 2px solid white;"> </img>
                                                         <div class="col-sm-12" style="margin-top: 3%;">
                                                          <a href="'.BASE_URL.'cimri/profil/&id='.$stanovalec['Upo']['id_uporabnik'].'">  <span class="label" style="font-size:14px; color: #333;">'.$stanovalec['Upo']['ime'].'</span> </a>
                                                        </div> ';?> 
                                        </div>
                                    </div>

                            <?php } 

                            }
                        }
                            else {?>


                            <h4 class="text-left" style="margin-top:5%">Na stanovanje še ni dodanih uporabnikov.</h4>


                        <?php }?>

                       
             </div>
            
			<div class="col-xs-12 col-sm-12 col-md-12 excerpet" style="padding-top:3%;padding-bottom:3%">
				         
            	<h4 class="text-left">Sobe:</h4>
                
                <?php foreach($sobe_izpis as $soba){  
				
				if($value['U']['id_stanovanje']==$soba['SO']['stanovanje_id_stanovanje']){?>
                    
                    <div class="col-sm-6 text-left" style="background-color:rgba(0,0,0,0.40); border:thin solid rgba(100,100,100,0.90);padding:15px 30px 15px 30px;">
                    
                    
                        <div class="col-sm-6">
                            <h4><span class="fa fa-info-circle"></span> Splošne informacije</h4>
                            <strong> Število postelj (prazne):</strong> <h4 style="background-color:#46AF4E;padding:5px 20px 5px 40px;"><?php echo $soba['SO']['stevilo_praznih_mest'];?>/ <?php echo $soba['SO']['stevilo_postelj'];?></h4>
                            
                            <strong> Velikost:</strong> <h4><?php echo $soba['SO']['velikost'];?></h4>
                            <strong> Željeni spol:</strong> 
                            <h4>
							<?php 
							if ($soba['SO']['zeljeni_spol']=="z"){ ?>
                            
                            Žensko
								
							<?php } else { ?>
                            
                            Moško
								
							<?php }
							
							?>
                            </h4> 
                        </div>
                        <div class="col-sm-6">  
                            <h4><span class="fa fa-euro"></span> Plačilo</h4>
                            <strong> Najemnina:</strong> <?php echo $soba['SO']['najemnina_vrednost'];?><span class="fa fa-euro"></span> <br />
                            <strong> Stroški:</strong> <?php echo $soba['SO']['stroski_vrednost'];?><span class="fa fa-euro"></span> <br />
                            <strong> Stroški (vrsta):</strong> <?php echo $soba['SO']['velikost'];?> <br />
                         
                            <hr class="hr-dark">
                            
                            <h4><span class="fa fa-list-ul"></span> Opis</h4>
                            <strong> Oprema:</strong> <?php echo $soba['SO']['oprema'];?> <br />
                            <strong> Opis:</strong> <?php echo $soba['SO']['opis_sobe'];?> <br />
                        </div>
                    </div>
              		
                    
                    <?php }}?>
                       
             </div>
            
		</article>
       <!-- konec stanovanje -->
        
            <hr class="hr-dark">    

            <?php } } ?>
        	</div>
        
        </div>
               
 	</div>
    
   <div class="col-xs-12" style="height:120px;"></div>
</section>                   
             