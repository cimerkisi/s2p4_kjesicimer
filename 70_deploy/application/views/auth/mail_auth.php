<section class="container-fluid naslovna_image col-sm-12 col-xs-12">

	<div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">AUTORIZACIJA</h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> REGISTRACIJA </h3>
    </div>
    
    <div class="hidden-lg hidden-md hidden-sm" style="height:70px;"></div>
       
    
    <!-- CIMER REGISTRACIJA BOX -->
    <div class="container col-md-8 col-sm-12 text-center col-md-push-1" style="margin-top:10%;background-color: #333; opacity: 0.9;padding-bottom:1%;margin-bottom:10%">
             
            
            <!-- forma za registracijo -->
            <div class="col-md-12 text-white registracija">
            
            <h2 class="text-left">  Potrditev e-maila</h2>
            	<hr class="hr-dark">
            
            	<form class="form-horizontal" method="POST" action="<?php echo BASE_URL . "home/prijava/"; ?>">
                
                	<fieldset>
                    
                    <!-- forma -->
                    
                    </fieldset>
                    
                    
           
            <button type="submit" class="btn btn-outline btn-violcno" style="float:right"><i class="fa fa-check" aria-hidden="true"></i> Prijava</button>
                
                </form>
            
	        </div>
            
    </div>
    
</section>

    <!-- SKRIPTA ZA PREDOGLED SLIK -->
            <script>
            function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#slika').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#galerija_slik").change(function(){
        readURL(this);
    });
        </script> 
 <!-- KONEC PREDOGLED SLIK -->
