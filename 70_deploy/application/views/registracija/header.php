<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>

<!-- Bootstrap -->
<link href="<?php echo BASE_URL; ?>public/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo BASE_URL; ?>public/css/bootstrap-social.css" rel="stylesheet">
<!-- jQuery -->
<script src="<?php echo BASE_URL; ?>public/js/jquery.js"></script>
<!-- Style -->
<link href="<?php echo BASE_URL; ?>public/css/style.css" rel="stylesheet">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,600" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet" type="text/css">

<!-- Font awesome -->
<link rel="stylesheet" href="<?php echo BASE_URL; ?>public/fonts/font-awesome-4.6.3/css/font-awesome.min.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
<!--          DODANO             -->

<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Isolated Version of Bootstrap, not needed if your site already used Bootstrap -->
<link rel="stylesheet"
	href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

<!-- HTML input type FILE
http://www.abeautifulsite.net/whipping-file-inputs-into-shape-with-bootstrap-3/ -->
<style>
.btn-file {
	position: relative;
	overflow: hidden;
}

.btn-file input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	min-width: 100%;
	min-height: 100%;
	font-size: 100px;
	text-align: right;
	filter: alpha(opacity = 0);
	opacity: 0;
	outline: none;
	background: white;
	cursor: inherit;
	display: block;
}
</style>

<!--      ///konec-DODANO      -->



</head>
<body>
<?php
	echo $menu;
?>
