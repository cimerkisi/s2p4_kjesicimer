<section class="container-fluid naslovna_image_autoH col-sm-12 col-xs-12" style="padding:0">

	<div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">REGISTRACIJA</h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> REGISTRACIJA </h3>
    </div>
    
    <div class="hidden-lg hidden-md hidden-sm" style="height:70px;"></div>
       
    
    <!-- CIMER REGISTRACIJA BOX -->
    <div class="container col-md-8 col-sm-12 text-center col-md-push-1" style="margin-top:10%;background-color: #333; opacity: 0.9;padding-bottom:1%;margin-bottom:10%">
             
             <!-- registracijski boxi -->
            <div class="row text-white">
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>1</h1>
               <small> Osebni podatki</small>
               </div>
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>2</h1>
               <small> Splošni podatki</small>
               </div>
               <div class="box-registration-step-active col-md-3 col-sm-3"><h1>3</h1>
               <small> Opis idealnega cimra</small>
               </div>
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>4</h1>
               <small> Potrditev e-maila</small>
               </div>
           	</div>
            
            <!-- forma za registracijo -->
            <div class="col-md-12 text-white registracija">
            
            <h2 class="text-left">  Opis idealnega cimra</h2>
            	<hr class="hr-dark">
            
            	<form class="form-horizontal" method="POST" action="<?php echo BASE_URL . "registracija/potrditev/" ?>">
                
                	<fieldset>
                    
		<?php
			foreach($_POST as $key=>$value) {
				echo '<input type="text" name="'.$key.'" value="'.$value.'" hidden>';
			}
		?>

                    <div class="form-group">
                         <label class="control-label col-sm-3">Iscem cimra...</label>
                          <div class="col-sm-7">
                                    <select class="form-control"
                                        name="cimer_s_sobo">
                                        <option>s sobo</option>
                                        <option>brez sobe</option>
                                        <option>vseeno</option>
                                    </select>
                            </div>

                    </div>



                    
                   <div class="form-group">
                        
                        <label class="control-label col-sm-3">Spanje:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="cimer_spanje">
                            <option selected>jutranja ptica</option>
                            <option>nocna ptica</option>
                            <option>Kaj je to - spanje?</option>
                        </select>
                        </div>
                        
                    </div>

                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Spol:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="cimer_spol">
                        	<option value="zenski" selected>Ženski</option>
                        	<option value="moski" >Moški</option>
                        	<option value="manj pomembno">manj pomembno</option>
                        </select>
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Kadilec:</label>
                        <div class="col-sm-7">
                         <select class="form-control" name="cimer_kadilec">
                        	<option selected>Ja</option>
                        	<option>Ne</option>
                        	<option>manj pomembno</option>
                        </select>
                        </div>
                        
                    </div>



                    
                     <!--  STYLE ZA RANGE SLIDER ???  -  ne dobi iz ../public/css/style.css-->


                        <div class="form-group">

                             <label class="control-label col-sm-3">Čistoča:</label>
                            <div class="col-sm-7">
                           

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="left" style=" font-size: small;">
                                        <label> Nesnazen</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="right" style=" font-size: small;">
                                        <label> Clean-freak</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <input type="range" name="cimer_cistoca" min="1" max="5" style="" >
                                </div>

                        
                            </div>

                        </div>


              

                          <div class="form-group">

                             <label class="control-label col-sm-3">Druzabnost:</label>
                            <div class="col-sm-7">
                           

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="left" style=" font-size: small;">
                                        <label> Samotar</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="right" style=" font-size: small;">
                                        <label>Zelo druzaben</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <input type="range" name="cimer_druzabnost" min="1" max="5" style="" >
                                </div>

                        
                            </div>

                        </div>
                    





                    
                    </fieldset>

                 <button type="submit" class="btn btn-outline btn-success" style="float:right"><i class="fa fa-check" aria-hidden="true"></i>Naslednja</button>
            
                </form>
            
	        </div>
           
           
    </div>

   <div class="col-xs-12" style="height:120px;"></div>
</section>
    <!-- SKRIPTA ZA PREDOGLED SLIK -->
            <script>
            function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#slika').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#galerija_slik").change(function(){
        readURL(this);
    });
        </script> 
 <!-- KONEC PREDOGLED SLIK -->
