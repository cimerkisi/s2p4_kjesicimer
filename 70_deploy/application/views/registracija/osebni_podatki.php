<section class="container-fluid naslovna_image_autoH col-sm-12 col-xs-12" style="padding:0">

	<div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">REGISTRACIJA</h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> REGISTRACIJA </h3>
    </div>
    
    <div class="hidden-lg hidden-md hidden-sm" style="height:70px;"></div>
       
    
    <!-- CIMER REGISTRACIJA BOX -->
    <div class="container col-md-8 col-sm-12 text-center col-md-push-1" style="margin-top:10%;background-color: #333; opacity: 0.9;padding-bottom:1%;margin-bottom:10%">
             
             <!-- registracijski boxi -->
	<?php if($reg_data['tip'] == 1) { ?>
            <div class="row text-white">
               <div class="box-registration-step-active col-md-6 col-sm-6"><h1>1</h1>
               <small> Osebni podatki</small>
               </div>
               <div class="box-registration-step col-md-6 col-sm-6 hidden-xs"><h1>2</h1>
               <small> Potrditev e-maila</small>
               </div>
             </div>
	<?php } else { ?>
            <div class="row text-white">
               <div class="box-registration-step-active col-md-3 col-sm-3"><h1>1</h1>
               <small> Osebni podatki</small>
               </div>
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>2</h1>
               <small> Splošni podatki</small>
               </div>
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>3</h1>
               <small> Opis idealnega cimra</small>
               </div>
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>4</h1>
               <small> Potrditev e-maila</small>
               </div>
             </div>
	<?php } ?>
            <!-- forma za registracijo -->
            <div class="col-md-12 text-white registracija">
            
            <h2 class="text-left">  Osebni podatki</h2>
            	<hr class="hr-dark">
            
	<?php if($reg_data['tip'] == 1) { ?>
            	<form class="form-horizontal" method="POST" action="<?php echo BASE_URL . "registracija/potrditev/" ?>">
	<?php } else { ?>
            	<form class="form-horizontal" method="POST" action="<?php echo BASE_URL . "registracija/splosni_podatki/" ?>">
	<?php } ?>
                
                	<fieldset>
                    
                    <!--<div class="form-group">
                    <label class="control-label col-sm-3">Izberite sliko:</label>
                    <img id="slika" src="#" class="img-responsive img-thumbnail" alt="Prikaz slike"/>
            				<label id="galerija_slik" hidden="hidden"> Izberite sliko:</label>
                            <div class="col-sm-4">
            					<input onchange="readURL(this);" type="file" name="img_source" accept="image/*" class="file btn btn-outline btn-violcno" oninvalid="this.setCustomValidity('Potrebno je dodati vsaj sliko!')" oninput="setCustomValidity('')" required>
                            </div>
                    </div>-->
		<?php
			foreach($_POST as $key=>$value) {
				echo '<input type="text" name="'.$key.'" value="'.$value.'" hidden>';
			}
		?>
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Ime:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="text" name="ime" required>
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Priimek:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="text" name="priimek" required>
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Geslo:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="password" name="geslo" id="geslo" required>
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Geslo (ponovitev):</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="password" name="geslo2" id="geslo2" onkeyup="checkPass(); return false;" required>
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">E-mail:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="email" name="mail" required>
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3" for="email">Spol:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="spol" required>
                        	<option value="zenski" selected>Ženski</option>
                        	<option value="moski">Moški</option>
                        	<option value="drugo">Drugo</option>
                        </select>
                        </div>
                        
                    </div>
                                        
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3" for="email">Datum rojstva:</label>
                        <div class="col-sm-7">
						<input class="form-control" id="date" placeholder="MM.DD.YYYY" type="text" name="datum_rojstva" required>
                        </div>
                        
                    </div>
                    
														
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3" for="email">Telefonska številka:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="tel" name="telefonska_stevilka">
                        </div>
                        
                    </div>
                  
                    
                    </fieldset>
            
		<button id="btn" type="submit" class="btn btn-outline btn-success" style="float:right"><i class="fa fa-check" aria-hidden="true"></i>Naslednja</button>
                
                </form>
            
	        </div>
           
            
    </div>

   <div class="col-xs-12" style="height:120px;"></div>
</section>

    <!-- SKRIPTA ZA PREDOGLED SLIK -->
            <script>
            function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#slika').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#galerija_slik").change(function(){
        readURL(this);
    });
        </script> 
 <!-- KONEC PREDOGLED SLIK -->
 
 <!-- PREVERJANJE GESLA -->
 
 	<script>
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('geslo');
    var pass2 = document.getElementById('geslo2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
	var btn=document.getElementById('btn');
	btn.disabled=true;
    if(pass1.value == pass2.value){
		btn.disabled = false;
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
		btn.disabled = true;
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}
	</script>
