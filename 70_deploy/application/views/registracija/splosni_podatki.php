<section class="container-fluid naslovna_image_autoH col-sm-12 col-xs-12" style="padding:0">

	<div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">REGISTRACIJA<?php echo $todo; ?></h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> REGISTRACIJA </h3>
    </div>
    
    <div class="hidden-lg hidden-md hidden-sm" style="height:70px;"></div>

    
    <!-- CIMER REGISTRACIJA BOX -->
    <div class="container col-md-8 col-sm-12 text-center col-md-push-1" style="margin-top:10%;background-color: #333; opacity: 0.9;padding-bottom:1%;margin-bottom:10%">
             
             <!-- registracijski boxi -->
            <div class="row text-white">
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>1</h1>
               <small> Osebni podatki</small>
               </div>
               <div class="box-registration-step-active col-md-3 col-sm-3"><h1>2</h1>
               <small> Splošni podatki</small>
               </div>
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>3</h1>
               <small> Opis idealnega cimra</small>
               </div>
               <div class="box-registration-step col-md-3 col-sm-3 hidden-xs"><h1>4</h1>
               <small> Potrditev e-maila</small>
               </div>
           	</div>
            
            <!-- forma za registracijo -->
            <div class="col-md-12 text-white registracija">
            
            <h2 class="text-left">  Splošni podatki</h2>
            	<hr class="hr-dark">
            
            	<form class="form-horizontal" method="POST" action="<?php echo BASE_URL . "registracija/opis_idealnega_cimra/"; ?>">
                
                	<fieldset>
                    
		<?php
			foreach($_POST as $key=>$value) {
				echo '<input type="text" name="'.$key.'" value="'.$value.'" hidden>';
			}
		?>
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Domači kraj:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="text" name="domaci_kraj">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Izobrazba:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="text" name="izobrazba">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Zaposlitev:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="text" name="zaposlitev">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Razmerje:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="razmerje">
                        	<option selected>V razmerju</option>
                        	<option>Samski/a</option>
                        	<option>Vdova/ec</option>
                        </select>
                        </div>
                        
                    </div>
                    
           			<h2 class="text-left">  Jaz kot cimer</h2>
                    <hr class="hr-dark">
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Interesi/hobiji:</label>
                        <div class="col-sm-7">
                        	<input class="form-control" type="text" name="hobiji">
                        </div>
                        
                    </div>


                     <div class="form-group">
                        
                        <label class="control-label col-sm-3">Glasba:</label>
                        <div class="col-sm-7">
                            <input class="form-control" type="text" name="glasba" placeholder="klasicna glasba, jazz, metal, tisina...">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Kuham:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="kuham">
                        	<option selected>redko</option>
                            <option>nikoli</option>
                        	<option>obcasno</option>
                        	<option>pogosto</option>
                        </select>
                        </div>
                        
                    </div>
                                        
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Prehrana:</label>
                        <div class="col-sm-7">
                            <input class="form-control" type="text" name="prehrana" placeholder="vegetarijanec, vegan, vsejedec, kanibal, alergije..." >
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Kadilec:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="kadilec">
                        	<option selected>da</option>
                        	<option>ne</option>
                        </select>
                        </div>
                        
                    </div>


                    <div class="form-group">
                        
                        <label class="control-label col-sm-3">Hisni ljubljencki:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="ljubljencki">
                            <option selected>imam</option>
                            <option>nimam</option>
                        </select>
                        </div>
                        
                    </div>
                    
                   
                        <div class="form-group">
                        
                        <label class="control-label col-sm-3">Spanje:</label>
                        <div class="col-sm-7">
                        <select class="form-control" name="spanje">
                            <option selected>jutranja ptica</option>
                            <option>nocna ptica</option>
                            <option>Kaj je to - spanje?</option>
                        </select>
                        </div>
                        
                    </div>




                <!--  STYLE ZA RANGE SLIDER ???  -  ne dobi iz ../public/css/style.css-->


                        <div class="form-group">

                             <label class="control-label col-sm-3">Čistoča:</label>
                            <div class="col-sm-7">
                           

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="left" style=" font-size: small;">
                                        <label> Nesnazen</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="right" style=" font-size: small;">
                                        <label> Clean-freak</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <input type="range" name="cistoca" min="1" max="5" style="" >
                                </div>

                        
                            </div>

                        </div>


              

                          <div class="form-group">

                             <label class="control-label col-sm-3">Druzabnost:</label>
                            <div class="col-sm-7">
                           

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="left" style=" font-size: small;">
                                        <label> Samotar</label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-xs-6" align="right" style=" font-size: small;">
                                        <label>Zelo druzaben</label>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <input type="range" name="druzabnost" min="1" max="5" style="" >
                                </div>

                        
                            </div>

                        </div>
                    
                  

                    <?php
			if($reg_data["tip"] == 2) {
		    ?> 
                    
                    <h2 class="text-left"> Stanovanje kakšno želim</h2>
                    <hr class="hr-dark">

                     <div class="form-group">
                        
                        <label class="control-label col-sm-3">Kraj nastanitve</label>
                        <div class="col-sm-7">
                            <input class="form-control" type="text" name="kraj_nastanitve">
                        </div>
                        
                    </div>

                    
                    <div class="form-group">
                    	

                        <label class="control-label col-sm-3">Vselitveno obdobje:</label>
                        <div class="col-sm-3">
                        	<input class="form-control"  id="date" placeholder="MM.DD.YYYY" type="text"  name="vselitveno_obdobje_od">
                        </div>
                         <label class="control-label col-sm-1" style="text-align: center"> - </label>
                        <div class="col-sm-3">
                            <input class="form-control"  id="date" placeholder="MM.DD.YYYY" type="text"  name="vselitveno_obdobje_do">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Cas najema v mesecih:</label>
                        <div class="col-sm-7">
                        	
                            <input type="number" name="cas_najema" class="form-control input-mini"
                                            placeholder="Nedoloceno" min="0" style="-moz-appearance:textfield;">
                            <!-- ce ne vpise nic, vrne prazno polje -->
                              

                        </div>
                        
                    </div>

                     <div class="form-group">
                        <label class="control-label col-sm-3">Budget</label>
                        <div class="col-sm-7">
                        <div class="input-group">
                        	  
                                    <input type="number" name="budget" class="form-control input-mini"
                                            placeholder="Nedoloceno" min="0" style="-moz-appearance:textfield;">
                                    <span class="input-group-addon"><i class="fa fa-eur"></i></span>
                         
                            
                        </div>
                        </div>
                    </div>


                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Opiši se kot cimra:</label>
                        <div class="col-sm-7">
                        	<textarea class="form-control" type="text" name="opis_jaz_kot_cimer"></textarea>
                        </div>
                        
                    </div>
                    
                    </fieldset>
			<?php } else if ($reg_data['tip'] == 3) { ?>
<!-- //////////////////////////////////////////////// -->                
                    
                    <h2 class="text-left"> Stanovanje kakšno imam</h2>
                    <hr class="hr-dark">
                    
                    <div class="form-group">
                        
                        <label class="control-label col-sm-3">Kraj nastanitve</label>
                        <div class="col-sm-7">
                            <input class="form-control" type="text" name="kraj_nastanitve">
                        </div>
                        
                    </div>
                    
                   <div class="form-group">
                    	

                        <label class="control-label col-sm-3">Vselitveno obdobje:</label>
                        <div class="col-sm-3">
                        	<input class="form-control" id="date" placeholder="MM.DD.YYYY" type="text"  name="vselitveno_obdobje_od">
                        </div>
                         <label class="control-label col-sm-1" style="text-align: center"> - </label>
                        <div class="col-sm-3">
                            <input class="form-control" name="vselitveno_obdobje_do"  id="date" placeholder="MM.DD.YYYY" type="text" >
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                    	
                        <label class="control-label col-sm-3">Cas najema v mesecih:</label>
                        <div class="col-sm-7">
                        	
                            <input type="number" name="cas_najema" class="form-control input-mini"
                                            placeholder="Nedoloceno" min="0" style="-moz-appearance:textfield;">
                            <!-- ce ne vpise nic, vrne prazno polje -->
                              

                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-sm-3">Cena</label>
                        <div class="col-sm-7">
                        <div class="input-group">
                        	  
                                    <input type="number" name="budget" class="form-control input-mini"
                                            placeholder="Nedoloceno" min="0" style="-moz-appearance:textfield;">
                                    <span class="input-group-addon"><i class="fa fa-eur"></i></span>
                         
                            
                        </div>
                        </div>
                    </div>
                    
                    </fieldset>
			<?php } ?>
<!-- //////////////////////////////////////////////// -->                
            <button type="submit" class="btn btn-outline btn-success" style="float:right"><i class="fa fa-check" aria-hidden="true"></i>Naslednja</button>
                </form>
            
	        </div>
           
            
    </div>

   <div class="col-xs-12" style="height:120px;"></div>
</section>
    <!-- SKRIPTA ZA PREDOGLED SLIK -->
            <script>
            function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#slika').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#galerija_slik").change(function(){
        readURL(this);
    });
        </script> 
 <!-- KONEC PREDOGLED SLIK -->
<?php
	echo $alert;
?>
