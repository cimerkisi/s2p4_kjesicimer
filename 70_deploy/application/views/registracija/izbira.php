<section class="container-fluid naslovna_image_autoH col-sm-12 col-xs-12" style="padding:0">

	<div class="container col-sm-2 col-xs-12" style="padding-top:3%;background-color: #333; opacity: 0.8;">
    	<h1 class="hidden-xs" style="float:right"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/></h1>
        
        <div class="hidden-lg hidden-md hidden-sm"> <img src="<?php echo BASE_URL; ?>public/html/image/logo.png" class="img-responsive" alt="Cimer-ki.si"/>
        <h4 class="text-white">REGISTRACIJA</h4>
        </div>
        
    	<h3 class="text-white text-right hidden-xs" style="padding-bottom:10%;font-size:2vw"> REGISTRACIJA </h3>
    </div>
    
    <div class="hidden-lg hidden-md hidden-sm" style="height:70px;"></div>
        
    
    <!-- CIMER REGISTRACIJA BOX -->
	<form name="tip2" method="POST" action="<?php echo BASE_URL . "registracija/osebni_podatki/"; ?>">
		<input type="text" name="tip" value="2" hidden>
	</form>
    <a href="javascript:void(0);" onclick="document.tip2.submit();" id="registracija_2">
    <div class="container col-md-2 col-sm-3 text-center col-md-push-1" style="margin-top:15%;background-color: #333; opacity: 0.8;padding-bottom:1%;">
            <h1 class="text-white text-center"> <span class="fa fa-user fa-3x"></span></h1>
            
            <h3 class="text-white text-center" style="padding-top:5%;font-size:3vh"> CIMER  </h3>
            
            <p class="text-white text-center">Si želiš najti pravo <strong>stanovanje/cimra zase?</strong> Izpolni podatke in prični z lovom! <br />
            <button type="button" class="btn btn-outline btn-success" id="btn_2"><i class="fa fa-check" aria-hidden="true"></i>Registracija</button></p>
            
    </div>
    </a>
    
	 <!-- CIMER Z STANOVANJEM REGISTRACIJA BOX -->
	<form name="tip3" method="POST" action="<?php echo BASE_URL . "registracija/osebni_podatki/"; ?>">
		<input type="text" name="tip" value="3" hidden>
	</form>
    <a href="javascript:void(0);" onclick="document.tip3.submit();" id="registracija_3">
    <div class="container col-md-2 col-sm-3 text-center col-md-push-2" style="margin-top:15%;background-color: #333; opacity: 0.8;padding-bottom:1%;">
            <h1 class="text-white text-center"> <span class="fa fa-user-plus fa-3x"></span></h1>
            
            <h3 class="text-white text-center" style="padding-top:5%;font-size:3vh"> CIMER S STANOVANJEM</h3>
            
            <p class="text-white text-center"> So te vsi cimri zapustili? Ne skrbi, <strong>mi ti pomagamo najti nove.</strong> <br />
            <button type="button" class="btn btn-outline btn-success" id="btn_3"><i class="fa fa-check" aria-hidden="true"></i>Registracija</button></p>
    </div>
    </a>

	    <!-- NAJEMODAJALEC REGISTRACIJA BOX -->
	<form name="tip1" method="POST" action="<?php echo BASE_URL . "registracija/osebni_podatki/"; ?>">
		<input type="text" name="tip" value="1" hidden>
	</form>
    <a href="javascript:void(0);" onclick="document.tip1.submit();" id="registracija_1">
    <div class="container col-md-2 col-sm-3  col-md-push-3 text-center" style="margin-top:15%;background-color: #333; opacity: 0.8;padding-bottom:1%;">
            <h1 class="text-white text-center"> <span class="fa fa-building fa-3x"></span></h1>
            
            <h3 class="text-white text-center" style="padding-top:5%;font-size:3vh"> NAJEMODAJALEC </h3>
            <p class="text-white text-center">Imaš stanovanje in ga želiš oddati?
            						<strong>Ni problema,</strong> sledi povezavi:<br /><br />
            <button type="button" class="btn btn-outline btn-violcno" id="btn_1"><i class="fa fa-check" aria-hidden="true"></i>Registracija</button></p>
    </div>
    </a>
    
    
   <div class="col-xs-12" style="height:120px;"></div>
</section>
<script>
	$("#registracija_1").hover(function(){
		$("#btn_1").attr("style", "background-color: #A42F8A;color: white;");
	}, function(){
		$("#btn_1").attr("style", "background-color: transparent;color: #A42F8A;");
	});
	$("#registracija_2").hover(function(){
		$("#btn_2").attr("style", "background-color: #5cb85c;color: white;");
	}, function(){
		$("#btn_2").attr("style", "background-color: transparent;color: #5cb85c;");
	});
	$("#registracija_3").hover(function(){
		$("#btn_3").attr("style", "background-color: #5cb85c;color: white;");
	}, function(){
		$("#btn_3").attr("style", "background-color: transparent;color: #5cb85c;");
	});
</script>
