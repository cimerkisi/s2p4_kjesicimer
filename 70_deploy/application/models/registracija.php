<?php

class Registracija extends Model {
	
	var $query_str1;
	var $query_str2;
	var $mail_code;
	function __construct() {

	}

	function osebni_podatki($data) {
		$geslo = $data['geslo'];
		$salt = randomString(50);
		$auth_code = randomString(50);
		$this->mail_code = $auth_code;
		$geslo = MD5($geslo . $salt);
		//insert into uporabnik(tip_uporabnika, ime, priimek, datum_rojstva, mail, geslo, telefonska_stevilka,spol) values(...)

		$datum_rojstva_temp = explode(".",$data['datum_rojstva']);
		$datum_rojstva = $datum_rojstva_temp[2]."-".$datum_rojstva_temp[1]."-".$datum_rojstva_temp[0];
		//$query_str = "INSERT INTO uporabnik (tip_uporabnika, ime, priimek, datum_rojstva, spol, mail, geslo, salt, telefonska_stevilka, auth_mail_code) VALUES ('".$data['tip']."', '".$data['ime']."', '".$data['priimek']."', '".$datum_rojstva."', '".$data['spol']."', '".$data['mail']."', '".$geslo."', '".$salt."', '".$data['telefonska_stevilka']."', '".$auth_code."')";
		$query_str = "INSERT INTO uporabnik (tip_uporabnika, ime, priimek, datum_rojstva, spol, mail, geslo, salt, telefonska_stevilka, auth_mail_code, dozvola_prijave) VALUES ('".$data['tip']."', '".$data['ime']."', '".$data['priimek']."', '".$datum_rojstva."', '".$data['spol']."', '".$data['mail']."', '".$geslo."', '".$salt."', '".$data['telefonska_stevilka']."', '".$auth_code."', '1')";
		$this->query_str1 = $query_str;
	}

	function __toString() {
		return $this->query_str1;
	}

	function returnMailCode() {
		return $this->mail_code;
	}
}

class Najmodavac extends Registracija {	
	function __construct($data) {
		$this->osebni_podatki($data);
	}
}

class Cimer extends Registracija {
	var $data;
	function __construct($data) {
		$this->osebni_podatki($data);
		$this->data = $data;

	}

	function lastnostiInsert($id) {
		$upisLa = new Baza();
		$this->query_str2 = "INSERT INTO lastnosti_uporabnik (id_uporabnika, domaci_kraj, izobrazba, zaposlitev, razmerje,hobiji, glasba, kuham,prehrana,kadilec,ljubljencki,spanje,cistoca,druzabnost,vselitveno_obdobje_od, vselitveno_obdobje_do, kraj_nastanitve,cas_najema,budget, opis_jaz_kot_cimer, cimer_s_sobo, cimer_spanje,cimer_spol,cimer_kadilec,cimer_cistoca,cimer_druzabnost) VALUES ('".$id."', '".$this->data['domaci_kraj']."', '".$this->data['izobrazba']."', '".$this->data['zaposlitev']."', '".$this->data['razmerje']."', '".$this->data['hobiji']."', '".$this->data['glasba']."', '".$this->data['kuham']."', '".$this->data['prehrana']."', '".$this->data['kadilec']."', '".$this->data['ljubljencki']."', '".$this->data['spanje']."', '".$this->data['cistoca']."', '".$this->data['druzabnost']."', '".$this->data['vselitveno_obdobje_od']."', '".$this->data['vselitveno_obdobje_do']."', '".$this->data['kraj_nastanitve']."', '".$this->data['cas_najema']."', '".$this->data['budget']."', '".$this->data['opis_jaz_kot_cimer']."', '".$this->data['cimer_s_sobo']."', '".$this->data['cimer_spanje']."', '".$this->data['cimer_spol']."', '".$this->data['cimer_kadilec']."', '".$this->data['cimer_cistoca']."', '".$this->data['cimer_druzabnost']."')";
		$upisLa->query($this->query_str2);
	}
}

class CimerStanovanje extends Registracija {
	var $data;
	function __construct($data) {
		$this->osebni_podatki($data);
		$this->data = $data;

	}

	function lastnostiInsert($id) {
		$upisLa = new Baza();
		$this->query_str2 = "INSERT INTO lastnosti_uporabnik (id_uporabnika, domaci_kraj, izobrazba, zaposlitev, razmerje,hobiji, glasba, kuham,prehrana,kadilec,ljubljencki,spanje,cistoca,druzabnost,vselitveno_obdobje_od, vselitveno_obdobje_do, kraj_nastanitve,cas_najema,budget, cimer_s_sobo, cimer_spanje,cimer_spol,cimer_kadilec,cimer_cistoca,cimer_druzabnost) VALUES ('".$id."', '".$this->data['domaci_kraj']."', '".$this->data['izobrazba']."', '".$this->data['zaposlitev']."', '".$this->data['razmerje']."', '".$this->data['hobiji']."', '".$this->data['glasba']."', '".$this->data['kuham']."', '".$this->data['prehrana']."', '".$this->data['kadilec']."', '".$this->data['ljubljencki']."', '".$this->data['spanje']."', '".$this->data['cistoca']."', '".$this->data['druzabnost']."', '".$this->data['vselitveno_obdobje_od']."', '".$this->data['vselitveno_obdobje_do']."', '".$this->data['kraj_nastanitve']."', '".$this->data['cas_najema']."', '".$this->data['budget']."', 'cimerStan_".$this->data['cimer_s_sobo']."', 'cimerStan_".$this->data['cimer_spanje']."', 'cimerStan_".$this->data['cimer_spol']."', 'cimerStan_".$this->data['cimer_kadilec']."', 'cimerStan_".$this->data['cimer_cistoca']."', 'cimerStan_".$this->data['cimer_druzabnost']."')";
		$upisLa->query($this->query_str2);
	}
}
